From d9f08d1e12a6d4dc66fdabe576bc79feb499abfd Mon Sep 17 00:00:00 2001
From: Fabian Boehm <FHomborg@gmail.com>
Date: Sat, 4 Feb 2023 18:57:41 +0100
Subject: Add workaround for Midnight Commander's issue with prompt extraction

When we draw the prompt, we move the cursor to the actual
position *we* think it is by issuing a carriage return (via
`move(0,0)`), and then going forward until we hit the spot.

This helps when the terminal and fish disagree on the width of the
prompt, because we are now definitely in the correct place, so we can
only overwrite a bit of the prompt (if it renders longer than we
expected) or leave space after the prompt. Both of these are benign in
comparison to staircase effects we would otherwise get.

Unfortunately, midnight commander ("mc") tries to extract the last
line of the prompt, and does so in a way that is overly naive - it
resets everything to 0 when it sees a `\r`, and doesn't account for
cursor movement. In effect it's playing a terminal, but not committing
to the bit.

Since this has been an open request in mc for quite a while, we hack
around it, by checking the $MC_SID environment variable.

If we see it, we skip the clearing. We end up most likely doing
relative movement from where we think we are, and in most cases it
should be *fine*.

Origin: upstream, https://github.com/fish-shell/fish-shell/pull/9540
Bug-Debian: https://bugs.debian.org/1035353
---

diff --git a/src/env_dispatch.cpp b/src/env_dispatch.cpp
index 28282f13ebd..9c3882ce02e 100644
--- a/src/env_dispatch.cpp
+++ b/src/env_dispatch.cpp
@@ -484,6 +484,14 @@ static void initialize_curses_using_fallbacks(const environment_t &vars) {
 // Apply any platform-specific hacks to cur_term/
 static void apply_term_hacks(const environment_t &vars) {
     UNUSED(vars);
+    // Midnight Commander tries to extract the last line of the prompt,
+    // and does so in a way that is broken if you do `\r` after it,
+    // like we normally do.
+    // See https://midnight-commander.org/ticket/4258.
+    if (auto var = vars.get(L"MC_SID")) {
+        screen_set_midnight_commander_hack();
+    }
+
     // Be careful, variables like "enter_italics_mode" are #defined to dereference through cur_term.
     // See #8876.
     if (!cur_term) {
diff --git a/src/screen.cpp b/src/screen.cpp
index b6e1ea8c38a..ef8fbf16ff3 100644
--- a/src/screen.cpp
+++ b/src/screen.cpp
@@ -75,6 +75,12 @@ static size_t try_sequence(const char *seq, const wchar_t *str) {
     return 0;  // this should never be executed
 }

+static bool midnight_commander_hack = false;
+
+void screen_set_midnight_commander_hack() {
+    midnight_commander_hack = true;
+}
+
 /// Returns the number of columns left until the next tab stop, given the current cursor position.
 static size_t next_tab_stop(size_t current_line_width) {
     // Assume tab stops every 8 characters if undefined.
@@ -905,7 +911,11 @@ void screen_t::update(const wcstring &left_prompt, const wcstring &right_prompt,

     // Also move the cursor to the beginning of the line here,
     // in case we're wrong about the width anywhere.
-    this->move(0, 0);
+    // Don't do it when running in midnight_commander because of
+    // https://midnight-commander.org/ticket/4258.
+    if (!midnight_commander_hack) {
+        this->move(0, 0);
+    }

     // Clear remaining lines (if any) if we haven't cleared the screen.
     if (!has_cleared_screen && need_clear_screen && clr_eol) {
diff --git a/src/screen.h b/src/screen.h
index 9c90fa44af8..26bbb452b62 100644
--- a/src/screen.h
+++ b/src/screen.h
@@ -330,4 +330,6 @@ class layout_cache_t : noncopyable_t {
 };

 maybe_t<size_t> escape_code_length(const wchar_t *code);
+
+void screen_set_midnight_commander_hack();
 #endif
